#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "$SCRIPT_DIR/back" || exit 1
./build.sh

cd "$SCRIPT_DIR/front/ZiraApp" || exit 2
./build.sh