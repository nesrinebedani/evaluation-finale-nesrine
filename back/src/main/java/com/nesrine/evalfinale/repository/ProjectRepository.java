package com.nesrine.evalfinale.repository;

import com.nesrine.evalfinale.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ProjectRepository extends JpaRepository<Project, Integer> {
// projects sorted by their creation date
    List<Project> findAllByOrderByCreationDateAsc();
}
