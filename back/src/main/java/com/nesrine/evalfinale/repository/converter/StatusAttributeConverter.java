package com.nesrine.evalfinale.repository.converter;

import com.nesrine.evalfinale.domain.Status;
import jakarta.persistence.AttributeConverter;

import jakarta.persistence.Converter;

import java.util.stream.Stream;


@Converter(autoApply = true)
public class StatusAttributeConverter
        implements AttributeConverter<Status, String> {

    @Override
    public String convertToDatabaseColumn(Status status) {
        if (status == null) {
            return null;
        }
        return status.getStatus();
    }

    @Override
    public Status convertToEntityAttribute(String status) {
        if (status == null) {
            return null;
        }

        return Stream.of(Status.values())
                .filter(c -> c.getStatus().equals(status))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}