package com.nesrine.evalfinale.repository;

import com.nesrine.evalfinale.domain.Project;
import com.nesrine.evalfinale.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TicketRepository extends JpaRepository <Ticket, Integer> {
    List<Ticket> findAllByOrderByCreationDateDesc();
    }


