package com.nesrine.evalfinale.domain.exception;

import com.nesrine.evalfinale.controller.dto.ErrorDto;
import com.nesrine.evalfinale.domain.exception.FunctionalException;
import com.nesrine.evalfinale.domain.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDto> onNotFoundException(NotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ErrorDto(e.getMessage()));
    }


    @ExceptionHandler(FunctionalException.class)
    public ResponseEntity<ErrorDto> onFunctionalException(FunctionalException e) {
        return ResponseEntity.badRequest()
                .body(new ErrorDto(e.getMessage()));
    }

    @ExceptionHandler({HttpMessageNotReadableException.class, TypeMismatchException.class, MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorDto> onInputException(Exception e) {
        return ResponseEntity.badRequest()
                .body(new ErrorDto(e.getMessage()));
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDto> onUnexpectedException(Exception e) {
        logger.error("Handler {}", e.getLocalizedMessage(), e);
        return ResponseEntity.internalServerError()
                .body(new ErrorDto("Unexpected error"));
    }
}
