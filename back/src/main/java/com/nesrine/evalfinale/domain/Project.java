package com.nesrine.evalfinale.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nesrine.evalfinale.domain.exception.InvalidTicketException;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")

    private String description;

    @Column(name = "created_at")

    private Instant creationDate;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)

    @OrderBy("status , creationDate DESC ")
    private List<Ticket> tickets = new ArrayList<>();

    public Project(Integer id, String name, String description, Instant creationDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
    }

    public Project() {
    }

    public void addTicket(Ticket ticket) {
        if (ticket == null) {
            throw new InvalidTicketException("Ticket is undefined");
        }

        // Save current date
        ticket.setCreationDate(Instant.now());

        tickets.add(ticket);
    }
}
