package com.nesrine.evalfinale.domain.exception;

public class NotFoundException extends FunctionalException {

    private final Integer id;
    private final Class<?> entityClass;


    public NotFoundException(Class<?> entityClass, Integer id) {
        super(entityClass.getSimpleName() + " with identifier " + id + " is not found");
        this.id = id;
        this.entityClass = entityClass;
    }

    public NotFoundException(Class<?> entityClass, Integer id, Throwable cause) {
        super(entityClass.getSimpleName() + " with identifier " + id + " is not found", cause);
        this.id = id;
        this.entityClass = entityClass;
    }



    public Integer getId() {
        return id;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }
}
