package com.nesrine.evalfinale.domain.exception;

public class CantGetAllException extends Exception {
    public CantGetAllException(String message) {
        super(message);
    }
}
