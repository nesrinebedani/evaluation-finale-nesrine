package com.nesrine.evalfinale.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "ticket")
public class Ticket {
    public Ticket() {
    }

    public Ticket(Integer id, String title, String content, Instant creationDate, Status status) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.creationDate = creationDate;
        this.status=status;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "title", nullable = false)
    @Size(max=50)
    private String title;

    @Column(name = "content",nullable = false)
    @Size(max=1000)
    private String content;

    @Column(name = "created_at")
    private Instant creationDate;

    @Column(name = "status")
    private Status status = Status.TODO;

    @Column(name="project_id")
    private Integer projectId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    @JoinColumn(name="project_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
private Project project;
}
