package com.nesrine.evalfinale.domain;

public enum Status {
    TODO("TODO"), INPROGRESS("INPROGRESS"), DONE("DONE");

    private String status;
    private Status(String status){
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
