package com.nesrine.evalfinale.controller.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nesrine.evalfinale.domain.Project;
import com.nesrine.evalfinale.domain.Ticket;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProjectDto {

    public static ProjectDto fromDomain(Project project) {
        ProjectDto dto = new ProjectDto();
        dto.setId(project.getId());
        dto.setName(project.getName());
        dto.setDescription(project.getDescription());

        dto.setCreationDate(project.getCreationDate());
        dto.setTickets(project.getTickets());

        return dto;
    }

    private Integer id;
    private String name;
    private  String description;
    private Instant creationDate;

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    private List<Ticket> tickets;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getCreationDate() {
        return creationDate;
    }


    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }


}
