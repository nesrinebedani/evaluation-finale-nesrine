package com.nesrine.evalfinale.controller.dto;

import com.nesrine.evalfinale.domain.Status;
import com.nesrine.evalfinale.domain.Ticket;

import java.time.Instant;

public class TicketInputDto {

    public Ticket toDomain() {
        return new Ticket(id, title, content, creationDate, status);
    }
    private Integer id;
    private String title;
    private String content;
    private Instant creationDate;
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }
}
