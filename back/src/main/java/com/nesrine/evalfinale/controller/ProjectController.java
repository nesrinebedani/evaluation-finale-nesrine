package com.nesrine.evalfinale.controller;

import com.nesrine.evalfinale.controller.dto.ProjectDto;
import com.nesrine.evalfinale.controller.dto.ProjectInputDto;
import com.nesrine.evalfinale.controller.dto.TicketDto;
import com.nesrine.evalfinale.controller.dto.TicketInputDto;
import com.nesrine.evalfinale.domain.Project;
import com.nesrine.evalfinale.domain.Ticket;
import com.nesrine.evalfinale.domain.exception.NotFoundException;
import com.nesrine.evalfinale.service.ProjectService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.Instant;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "/api/projects")
@ResponseStatus

public class ProjectController {


    private ProjectService projectService;

    public ProjectController(ProjectService service) {
        this.projectService = service;
    }

    @GetMapping("")
    public ResponseEntity<List<Project>> getAllProjects() {
        return ResponseEntity.ok(projectService.listProjects());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectDto> getProjectById(@PathVariable("id") Integer projectId) {
        Project project = projectService.getProject(projectId);

        return ResponseEntity.ok(ProjectDto.fromDomain(project));
    }

    @PostMapping
    public ResponseEntity<ProjectDto> createProject(@RequestBody @Valid ProjectInputDto dto) {
        dto.setCreationDate(Instant.now());
        Project createdProject = projectService.createProject(dto.toDomain());

        return ResponseEntity.created(URI.create("/projects/" + createdProject.getId()))
                .body(ProjectDto.fromDomain(createdProject));
    }

    @DeleteMapping("/{id}")
    public void deleteProject(@PathVariable String id) {
        projectService.deleteProject(Integer.parseInt(id));
    }


    @PostMapping("/{id}/tickets")
    public ResponseEntity<TicketDto> addTicket(@PathVariable("id") Integer projectId, @RequestBody @Valid TicketInputDto dto) {
        Ticket ticket = projectService.addTicket(projectId, dto.toDomain());

        return ResponseEntity.ok(TicketDto.fromDomain(ticket));
    }

    @GetMapping("/projects/{id}/tickets")
    public ResponseEntity<List<TicketDto>> getAllTicketsByProjectId(@PathVariable(value = "id") Integer projectId) {
        List<Ticket>  tickets=projectService.listAllTicketsByProjectId(projectId);

        return ResponseEntity.ok(tickets
                .stream()
                .map(TicketDto::fromDomain)
                .collect(toList()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProjectDto> updateProject(
            @PathVariable("id") Integer projectId,
            @RequestBody @Valid ProjectInputDto projectInputDto) {
        Project updatedProject = projectService.updateProject(projectId, projectInputDto.toDomain());

        return ResponseEntity.ok(ProjectDto.fromDomain(updatedProject));
    }


}



