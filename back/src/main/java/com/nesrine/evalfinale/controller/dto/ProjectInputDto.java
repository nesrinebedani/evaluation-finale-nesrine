package com.nesrine.evalfinale.controller.dto;

import com.nesrine.evalfinale.domain.Project;
import jakarta.validation.constraints.NotBlank;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.time.Instant;

public class ProjectInputDto {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }
    private Integer id;

    @NotBlank
    private String name;
    @NotBlank
    @Size(max=500)
    private String description;


     private Instant creationDate;


    public Project toDomain() {
        return new Project(id, name, description, creationDate);
    }
}
