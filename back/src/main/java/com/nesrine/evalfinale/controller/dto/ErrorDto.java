package com.nesrine.evalfinale.controller.dto;

import java.time.Instant;

public class ErrorDto {
    private final Instant timestamp = Instant.now();
    private final String message;

    public ErrorDto(String message) {this.message = message;}

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }
}
