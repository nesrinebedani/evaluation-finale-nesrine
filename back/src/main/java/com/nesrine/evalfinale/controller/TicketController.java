package com.nesrine.evalfinale.controller;

import com.nesrine.evalfinale.controller.dto.ProjectDto;
import com.nesrine.evalfinale.controller.dto.ProjectInputDto;
import com.nesrine.evalfinale.controller.dto.TicketDto;
import com.nesrine.evalfinale.controller.dto.TicketInputDto;
import com.nesrine.evalfinale.domain.Project;
import com.nesrine.evalfinale.domain.Ticket;
import com.nesrine.evalfinale.domain.exception.CantGetAllException;
import com.nesrine.evalfinale.service.ProjectService;
import com.nesrine.evalfinale.service.TicketService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "/api/tickets")
@ResponseStatus

public class TicketController {
    private ProjectService projectService;
    private TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;

    }

    @GetMapping("")
    public ResponseEntity<List<Ticket>> getAllTickets() {
        return (ResponseEntity.ok(ticketService.listTickets()));


    }

    @PutMapping("/{id}")
    public ResponseEntity<TicketDto> updateTicket(@PathVariable("id") Integer id, @RequestBody TicketInputDto ticketInputDto) {

        Ticket updatedTicket = ticketService.updateTicket(id, ticketInputDto.toDomain());

        return ResponseEntity.ok(TicketDto.fromDomain(updatedTicket));
    }

    @DeleteMapping("/{id}")
    public void deleteTicket(@PathVariable("id") Integer id) {
        ticketService.delete(id);

  }




}

