package com.nesrine.evalfinale.service;

import com.nesrine.evalfinale.domain.exception.CantGetAllException;
import com.nesrine.evalfinale.domain.Ticket;
import com.nesrine.evalfinale.domain.exception.NotFoundException;
import com.nesrine.evalfinale.repository.TicketRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class TicketService {
    private final TicketRepository ticketRepository;


    public TicketService(TicketRepository ticketRepository) {

        this.ticketRepository = ticketRepository;
    }

    public List<Ticket> listTickets() {
        return ticketRepository.findAllByOrderByCreationDateDesc();
    }

    public List<Ticket> getAll() throws CantGetAllException {
        if (ticketRepository.findAll().size() == 0) {
            throw new CantGetAllException("There is no ticket registered in the list");
        }
        return ticketRepository.findAll();
    }

    public Ticket getTicket(Integer ticketId) {
        return ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException(Ticket.class, ticketId));
    }

    @Transactional
    public Ticket updateTicket(Integer id, Ticket toDomain) {
        Ticket existingTicket = getTicket(id);

        existingTicket.setContent(toDomain.getContent());
        existingTicket.setStatus(toDomain.getStatus());

        return ticketRepository.save(existingTicket);
    }

    @Transactional
    public void delete(Integer id) {
        ticketRepository.deleteById(id);
    }


    @Transactional
    public Ticket createTicket(Ticket ticket) {
        return ticketRepository.save(ticket);
    }
    @Transactional
    public void deleteAllTicketsOfProject(Integer projectId) {
      ticketRepository.deleteById(projectId);
    }
}
