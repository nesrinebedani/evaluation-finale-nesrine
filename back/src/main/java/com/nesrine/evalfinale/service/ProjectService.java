package com.nesrine.evalfinale.service;

import com.nesrine.evalfinale.controller.dto.ProjectDto;
import com.nesrine.evalfinale.domain.Project;
import com.nesrine.evalfinale.domain.Ticket;
import com.nesrine.evalfinale.domain.exception.NotFoundException;
import com.nesrine.evalfinale.repository.ProjectRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> listProjects() {
        return projectRepository.findAllByOrderByCreationDateAsc();
    }

    public List<Ticket> listTickets(Integer projectId) {
        return projectRepository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(Project.class, projectId))
                .getTickets();
    }

    @Transactional
    public Project createProject(Project project) {
        return projectRepository.save(project);
    }

    public Project getProject(Integer projectId) {
        return projectRepository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(Project.class, projectId));
    }

    @Transactional
    public Ticket addTicket(Integer projectId, Ticket ticket) {
        Project project = getProject(projectId);

        project.addTicket(ticket);

        projectRepository.save(project);
        return ticket;

    }

    @Transactional
    public Project updateProject(Integer projectId, Project updatedProject) {
        Project existingProject = projectRepository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(Project.class, projectId));

        existingProject.setName(updatedProject.getName());
        existingProject.setDescription(updatedProject.getDescription());
        existingProject.setTickets(updatedProject.getTickets());
        existingProject.setCreationDate(updatedProject.getCreationDate());


        return projectRepository.save(existingProject);
    }

    @Transactional
    public void deleteProject(Integer projectId) {
        projectRepository.deleteById(projectId);


    }

    public List<Ticket> listAllTicketsByProjectId(Integer projectId) {
        return projectRepository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(Project.class, projectId))
                .getTickets();

    }
}