---Project
INSERT INTO project (name, description, author)
VALUES ('exam','urgent studing reading library', 'Bernard')
ON CONFLICT DO NOTHING;
INSERT INTO project (name, description,author)
VALUES ('shopping','consumption buying money credit', 'Barry White')
ON CONFLICT DO NOTHING;
INSERT INTO project (name, description, author)
VALUES ('meeting','business logic and comprehension', 'Ian')
ON CONFLICT DO NOTHING;
INSERT INTO project (name, description, author)
VALUES ('construction','dreams are for fool','Claire')
ON CONFLICT DO NOTHING;
--- Ticket
INSERT INTO ticket (title, content, author,project_id)
VALUES ('second','superlicious magnificous lalaland','Arina',1)
ON CONFLICT DO NOTHING;
INSERT INTO ticket (title, content,  author, project_id)
VALUES ('holiday','relaxing day on the beach ','Yassir',2)
ON CONFLICT DO NOTHING;
INSERT INTO ticket (title, content, author,  project_id)
VALUES ('familly','funny laughing crazy going chilling potatoes','Margaux',3)
ON CONFLICT DO NOTHING;