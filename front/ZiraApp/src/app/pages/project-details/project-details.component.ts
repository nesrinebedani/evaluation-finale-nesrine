import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ActivatedRoute } from '@angular/router';
import { ProjectsService } from 'src/app/services/projects-service';
import { firstValueFrom } from 'rxjs/internal/firstValueFrom';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  project: Project | undefined;

  constructor(private route: ActivatedRoute, private projectService: ProjectsService) {

  }

  ngOnInit(): void {
    this.loadProject();
  }
  private loadProject(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.projectService.getProjectById(id)
      .subscribe(project => this.project = project);

  }
  deleteProject(project: Project) {
    this.projectService.deleteProject(project.id).subscribe(() => {

    })
  }

  //recuperer un ticket de la bdd.voir wine details. 
}




