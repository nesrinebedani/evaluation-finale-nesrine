import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectsService } from 'src/app/services/projects-service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  projects: Project[] = [];

  constructor(private projectsService: ProjectsService) {
  }

  ngOnInit(): void {
    this.loadProjects();
  }

  private loadProjects() {

    this.projectsService.getProjects().subscribe(projects => {
      this.projects = projects;
      console.log(projects)
    })
  }

}
