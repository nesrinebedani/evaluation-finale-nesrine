import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProjectDto } from 'src/app/models/project';
import { ProjectsService } from 'src/app/services/projects-service';


@Component({
    selector: 'app-add-project',
    templateUrl: './add-project.component.html',
    styleUrls: ['./add-project.component.scss']

})

export class AddProjectComponent {
    constructor(
        private router: Router,
        private projectService: ProjectsService,
    ) { }
    form = new FormGroup({
        name: new FormControl('', [Validators.required]),
        description: new FormControl('', [Validators.required]),

    })
    submitForm() {
        if (this.form.valid) {
            const values = this.form.value as ProjectDto;
            this.projectService.addProject(values).subscribe((_) => {

                this.router.navigate(['/'])
            })
        }


    }
}


