import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {
    catchError,
    map,
    Observable, BehaviorSubject,
    race,
    Subject,
    tap,
    throwError,
} from 'rxjs'
import { Project, ProjectDto } from '../models/project';

@Injectable({
    providedIn: 'root'
})

export class ProjectsService {


    private projectsUrl = '/api/projects';  // URL to web api

    constructor(private http: HttpClient) {

    }

    getProjects(): Observable<Project[]> {

        return this.http.get<Project[]>(this.projectsUrl)

    }

    getProjectById(id: Number): Observable<Project> {
        return this.http.get<Project>(this.projectsUrl + '/' + id);
    }
    addProject(project: ProjectDto) {
        return this.http.post<Project>(this.projectsUrl, project);

    }
    deleteProject(id: number) {
        return this.http.delete<void>(`${this.projectsUrl}/${id}`)
    }

}

