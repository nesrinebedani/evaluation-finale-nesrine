import { Injectable } from "@angular/core";
import { Ticket } from "../models/ticket";
import { TicketDto } from "../models/ticketDto";
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from "rxjs";
import { tap } from "rxjs";



@Injectable({
    providedIn: 'root'
})

export class TicketService {

    public ticket$: Subject<Ticket> = new Subject<Ticket>()

    private ticketsUrl = '/api/tickets';  // URL to web api

    constructor(private http: HttpClient) {
    }
    createTicket(ticket: TicketDto): Observable<Ticket> {
        return this.http.post<Ticket>(this.ticketsUrl, ticket)
    }

    deleteTicket(id: number): Observable<void> {
        return this.http.delete<void>(`${this.ticketsUrl}/${id}`)
    }

    findById(id: number): Observable<Ticket> {
        return this.http.get<Ticket>(`${this.ticketsUrl}/${id}`)
    }
}