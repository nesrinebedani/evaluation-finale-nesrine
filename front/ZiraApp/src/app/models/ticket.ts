export interface Ticket {
    id: number,
    title: string,
    content: string,
    creationDate: string,
    status: string,
    projectId: bigint,

}