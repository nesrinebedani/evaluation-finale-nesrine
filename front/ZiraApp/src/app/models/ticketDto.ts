import { Project } from "./project"

export interface TicketDto {
    title: string,
    content: string,
    status: string,
    projectId: number
}
