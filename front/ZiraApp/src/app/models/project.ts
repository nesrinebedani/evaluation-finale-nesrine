import { Ticket } from "./ticket";

export interface Project {
    id: number,
    name: string,
    description: string,
    creationDate: number,
    tickets: Ticket[],
}
export interface ProjectDto {

    name: string,
    description: string,

}