#!/bin/bash

set -e

based_tag_name=${CI_REGISTRY_IMAGE:-nesrine/zira}


npm install
#npm run test
npm run build
docker build -t "$based_tag_name/front:latest" .
